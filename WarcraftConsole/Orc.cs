using System;

namespace Warcraft;
{
    public class Peon
    {
        private string race;
        private string type;
        private string faction;
        private string name;
        private int armor;
        private int hitpoints;

        


        public Dinosaur(string race, string name, string type,string faction, int armor, int hitpoints,)
        {
            this.name=name;
            this.race=race;
            this.type=type;
            this.faction=faction;
            this.hitpoints=hitpoints;
            this.armor=armor;
        }
        public string sayHello()
        {
            string phrase = string.Format("I'm {0}, I'm a {1} and my faction is the {2}.",this.name,this.type,this.faction);
            return phrase;
        }

        public string roar()
        {
            string grrr = string.Format("Grrr");
            return grrr;
        }

        public int getHitpoints()
        {
            return this.hitpoints;
        }
        public int setHitpoints(int newHitPoints)
        {
            this.hitpoints=newHitPoints;
        }
        
        public int getArmor()
        {
            return this.armor;
        }
        public void setArmor(int newArmor)
        {
            this.armor=newArmor;
        }
        public string getType()
        {
            return this.type;
        }
        public void setType(string newType)
        {
            this.type=newType;
        }
        public void getRace()
        {
           return this.race;
        }
        public void setRace(string newRace)
         {
             this.race=newRace;
         }

        public string getFaction()
        {
            return this.faction;
        }

        public void setFaction(string newFaction)
        {
            this.faction=newFaction; 
        }
        public string getName()
        {
            return this.name;
        }

        public void setName(string newName)
        {
            this.name=newName;
        }
        
        public string hug(Dinosaur Dinosaur)
        {
            string hug = string.Format("Je suis {0} et je fais un calin à {1}.", this.getName(), Dinosaur.getName());
            return hug;
        }
    }
}